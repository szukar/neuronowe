package main.java;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Model model = new Model(6, 7);
        Controller controller = new Controller(model);
        View view = new View(controller, model);

        Scene scene = new Scene(view.asParent());
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
