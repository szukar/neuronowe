package main.java;

public class Controller {

    private final Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void updateValue(int width, int height, Object value) {
        model.setValue(width, height, (Boolean) value);
    }
}
