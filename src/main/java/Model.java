package main.java;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class Model {
    private int width;
    private int height;
    private BooleanProperty[][] table;

    public Model(int width, int height) {
        this.width = width;
        this.height = height;

        init();
    }

    private void init() {
        table = new BooleanProperty[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                table[i][j] = new SimpleBooleanProperty();
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public BooleanProperty[][] getTable() {
        return table;
    }

    public void setTable(BooleanProperty[][] table) {
        this.table = table;
    }

    public Boolean getValue(int width, int height) {
        return table[width][height].getValue();
    }

    public void setValue(int width, int height, Boolean value) {
        table[width][height].setValue(value);
    }

    public Boolean[][] getValues() {
        Boolean[][] values = new Boolean[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                values[i][j] = table[i][i].getValue();
            }
        }
        return values;
    }

    public void setValues(Boolean[][] values) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                table[i][j].setValue(values[i][j]);
            }
        }
    }
}
