package main.java;

import javafx.beans.property.BooleanProperty;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class View {
    private Controller controller;
    private Model model;
    private HBox hBox;
    private GridPane gridPane;
    private CheckBox[][] checkBoxes;


    public View(Controller controller, Model model) {
        this.controller = controller;
        this.model = model;

        createAndConfigurePane();

        createAndLayoutControls();

        updateControllerFromListeners();

        observeModelAndUpdateControls();
    }

    private void updateIfNeeded(Boolean value, CheckBox checkBox) {
        if (!value.equals(checkBox.isSelected())) {
            checkBox.setSelected(value);
        }
    }

    public Parent asParent() {
        return gridPane ;
    }

    private void createAndLayoutControls() {
        checkBoxes = new CheckBox[model.getWidth()][model.getHeight()];

        for (int i = 0; i < model.getWidth(); i++) {
            for (int j = 0; j < model.getHeight(); j++) {
                CheckBox checkBox = new CheckBox();
                checkBox.setMinSize(30, 30);
                checkBoxes[i][j] = checkBox;
                gridPane.add(checkBox, i, j);
            }
        }
    }

    private void createAndConfigurePane() {
        gridPane = new GridPane();
        gridPane.setHgap(1);
        gridPane.setVgap(1);
    }

    private void updateControllerFromListeners() {
        for (int i = 0; i < model.getWidth(); i++) {
            for (int j = 0; j < model.getHeight(); j++) {
                int finalI = i;
                int finalJ = j;
                checkBoxes[i][j].pickOnBoundsProperty().bind((obs, oldV, newV) -> controller.updateValue(finalI, finalJ, newV)); //something don't work here
            }
        }
    }

    private void observeModelAndUpdateControls() {
        for (int i = 0; i < model.getWidth(); i++) {
            for (int j = 0; j < model.getHeight(); j++) {
                BooleanProperty property = model.getTable()[i][j];
                CheckBox checkBox = checkBoxes[i][j];
                property.addListener((obs, oldV, newV) -> updateIfNeeded(newV, checkBox));
            }
        }
    }
}
